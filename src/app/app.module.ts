import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { EmployerDrivesComponent } from './Employer/employer-drives/employer-drives.component';
import { InterviewDrivesComponent } from './interview/interview-drives/interview-drives.component';
import { InterviewerDriveDetailsComponent } from './interview/interviewer-drive-details/interviewer-drive-details.component';
import { RecruiterDriveCardsComponent } from './Recruiter/recruiter-drive-cards/recruiter-drive-cards.component';
import { RecruiterDriveDetailsComponent } from './Recruiter/recruiter-drive-details/recruiter-drive-details.component';
import { RecruiterCreateDriveComponent } from './Recruiter/recruiter-create-drive/recruiter-create-drive.component';
import { EmployerDriveDetailsComponent } from './Employer/employer-drive-details/employer-drive-details.component';
import { HeaderComponent } from './header/header.component';
import { RegistrationComponent } from './registration/registration.component';
import { ProfileComponent } from './profile/profile.component';
import { InterviewgetfeedbackComponent } from './interview/interviewgetfeedback/interviewgetfeedback.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    EmployerDrivesComponent,
    InterviewDrivesComponent,
    InterviewerDriveDetailsComponent,
    RecruiterDriveCardsComponent,
    RecruiterDriveDetailsComponent,
    RecruiterCreateDriveComponent,
    EmployerDriveDetailsComponent,
    HeaderComponent,
    RegistrationComponent,
    ProfileComponent,
    InterviewgetfeedbackComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
 