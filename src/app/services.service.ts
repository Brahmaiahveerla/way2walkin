import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})

export class ServicesService {

  url: string = 'http://192.168.150.70:8080/api/';
  url1: string = 'http://192.168.150.70:8080/api/';
  private recuriterDriveUrl = 'http://192.168.150.70:8080/api/recruiter/drives';
  private recuriterDriveById = 'http://192.168.150.70:8080/api/drives';
  private driveDetailsBYId = 'http://192.168.150.70:8080/api/employer/getInterviews';
  private getEmployeers = 'http://192.168.150.70:8080/api/employer/getAllEmployees';
  private newcreateDrive = 'http://192.168.150.70:8080/api/create/drives';

  constructor(private http: HttpClient) { }

  doLogin(data: any) {
    return this.http.post<any>(this.url + 'login', data)
  }


  getEmployerDrives() {
    const httpOptions = { headers: new HttpHeaders({ 'content-type': 'application/json', 'Authorization': localStorage.getItem('jwt-token') }) };
    return this.http.get<any>(this.url + '/drives/employer/' + localStorage.getItem('userId'), httpOptions)
  }

  getDriveCandidates(id: number) {
    const httpOptions = { headers: new HttpHeaders({ 'content-type': 'application/json', 'Authorization': localStorage.getItem('jwt-token') }) };
    return this.http.get<any>(this.url + 'employer/getInterviews/' + id + '/' + localStorage.getItem('userId'), httpOptions)
  }

  getDriveDetails(id: number) {
    const httpOptions = { headers: new HttpHeaders({ 'content-type': 'application/json', 'Authorization': localStorage.getItem('jwt-token') }) };
    return this.http.get<any>(this.url + 'drives/' + id + '/' + localStorage.getItem('userId'), httpOptions)
  }

  getprofile() {

    const httpOptions = { headers: new HttpHeaders({ 'content-type': 'application/json', 'Authorization': localStorage.getItem('jwt-token') }) };
    return this.http.get<any>(this.url + 'GetUserInfo/' + localStorage.getItem('userId'), httpOptions)
  }

  getInterviewers() {
    const httpOptions = { headers: new HttpHeaders({ 'content-type': 'application/json', 'Authorization': localStorage.getItem('jwt-token') }) };
    return this.http.get<any>(this.url + 'drives/interviewer/' + localStorage.getItem('userId'), httpOptions)
  }
  getInterviewersDriveDetails(id: number) {
    const httpOptions = { headers: new HttpHeaders({ 'content-type': 'application/json', 'Authorization': localStorage.getItem('jwt-token') }) };
    return this.http.get<any>(this.url + 'drives/' + id + '/' + localStorage.getItem('userId'), httpOptions)
  }
 




  //service methods for recruitermodule
   //service method for get all recuriter drives
   getRecuriterDrives() {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization':  localStorage.getItem('jwt-token') }) };
    const url = `${this.recuriterDriveUrl}/${localStorage.getItem("userId")}`;
    return this.http.get<any>(url, httpOptions);
  }

  //service method for recuriter drive details
  getDrivesById(id : any){
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization':  localStorage.getItem('jwt-token')}) };
    const url = `${this.recuriterDriveById}/${id}/${localStorage.getItem("userId")}`;
    return this.http.get<any>(url, httpOptions);
  }


  //service method for recuriter drive footer
   getCompletedInerviews(id : any){
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization':  localStorage.getItem('jwt-token') }) };
    const url = `${this.driveDetailsBYId}/${id}/${localStorage.getItem("userId")}`;
    return this.http.get<any>(url,httpOptions);
   }
  
  //service method for get all employeers
   getAllEmployeers() : any{
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization':  localStorage.getItem('jwt-token') }) };
    const url = `${this.getEmployeers}`;
    return this.http.get<any>(url,httpOptions);
   }

   //service method for create drive
   createnewDrive(userObject : any) : any{
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization':  localStorage.getItem('jwt-token') }) };
    const url = `${this.newcreateDrive}/${localStorage.getItem("userId")}`;
    return this.http.post<any>(url,userObject,httpOptions);
   }

} 


  



  

  


 


