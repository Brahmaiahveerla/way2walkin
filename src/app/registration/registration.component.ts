import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServicesService } from '../services.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false; 
  constructor(private formBuilder: FormBuilder, private service: ServicesService, private router: Router) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstname: ['', [Validators.required, Validators.minLength(6)]],
      lastname: ['', [Validators.required, Validators.minLength(6)]],
      emailId: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      dateofbirth: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      Skills: ['', [Validators.required]],
      language: ['', [Validators.required]],
      PhoneNo: ['', [Validators.required, Validators.minLength(10)]],
      fileToUpload: ['', [Validators.required]],
    });
  }
  get f() { return this.registerForm.controls; }
 
 

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    console.log(this.registerForm.value )
  }


}
