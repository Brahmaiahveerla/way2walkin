import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  token: boolean;
  constructor(private router: Router) { }

  logout() {
    if(window.confirm("Do you want Logout?")) {
      localStorage.removeItem('jwt-token');
      localStorage.removeItem('Role');
      localStorage.removeItem('userId');
      this.token = false;
      this.router.navigate(['/login']) ;
    }
  }
  ngOnInit() {

    if (localStorage.getItem('jwt-token')) {
      this.token = true;
    // } else {
    //   this.token = false;
    // }
    }
  }

}
