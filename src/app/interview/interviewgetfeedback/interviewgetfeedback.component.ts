import { Component, OnInit , ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ServicesService } from 'src/app/services.service';


@Component({
  selector: 'app-interviewgetfeedback',
  templateUrl: './interviewgetfeedback.component.html',
  styleUrls: ['./interviewgetfeedback.component.css']
})
export class InterviewgetfeedbackComponent implements OnInit {
  drives: any;
  registrations: any;
  id: number;
  constructor(private activatedRoute: ActivatedRoute, private service: ServicesService) { }

  ngOnInit() {
    
    this.activatedRoute.queryParams.subscribe(params => {
      this.id = params['id'];
    })
    this.service.getInterviewersDriveDetails(this.id).subscribe((data) => {

      this.drives = data.response;
      this.registrations = data.response.registrations;
     
    })

  }

}
