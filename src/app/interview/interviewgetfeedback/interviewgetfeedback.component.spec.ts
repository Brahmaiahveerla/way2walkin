import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterviewgetfeedbackComponent } from './interviewgetfeedback.component';

describe('InterviewgetfeedbackComponent', () => {
  let component: InterviewgetfeedbackComponent;
  let fixture: ComponentFixture<InterviewgetfeedbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterviewgetfeedbackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterviewgetfeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
