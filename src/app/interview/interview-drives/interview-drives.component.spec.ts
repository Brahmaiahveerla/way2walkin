import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterviewDrivesComponent } from './interview-drives.component';

describe('InterviewDrivesComponent', () => {
  let component: InterviewDrivesComponent;
  let fixture: ComponentFixture<InterviewDrivesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterviewDrivesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterviewDrivesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
