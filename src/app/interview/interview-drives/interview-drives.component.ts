import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/services.service';
@Component({
  selector: 'app-interview-drives',
  templateUrl: './interview-drives.component.html',
  styleUrls: ['./interview-drives.component.css']
})
export class InterviewDrivesComponent implements OnInit {
  drives: any;
  completedInterviews: any;
  scheduledInterviews: any;

  constructor(private service: ServicesService) { }

  ngOnInit() {
    this.service.getInterviewers().subscribe((data) => {
      this.completedInterviews = data.response.completedInterviews;
      this.scheduledInterviews = data.response.scheduledInterviews;
     
    })
  }
}
