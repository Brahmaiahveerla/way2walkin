export class Feedback{
    FeedBackComment:string;
    IsRecommended:Boolean;
    MaximumRating:number;
    overallComments:string;
    skillname:string;
    videourl:string;
}