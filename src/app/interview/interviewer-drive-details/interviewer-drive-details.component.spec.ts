import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterviewerDriveDetailsComponent } from './interviewer-drive-details.component';

describe('InterviewerDriveDetailsComponent', () => {
  let component: InterviewerDriveDetailsComponent;
  let fixture: ComponentFixture<InterviewerDriveDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterviewerDriveDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterviewerDriveDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
