import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ServicesService } from 'src/app/services.service';
import { Feedback } from '../interviewer-drive-details/giveFeedBackForm'

@Component({
  selector: 'app-interviewer-drive-details',
  templateUrl: './interviewer-drive-details.component.html',
  styleUrls: ['./interviewer-drive-details.component.css']
})
export class InterviewerDriveDetailsComponent implements OnInit {
  drives: any;
  registrations: any;
  id: number;
  feedback: Feedback;

  FeedBackComment: string = "";
  overallComments: string = "";
  skillname: string = "";
  videourl: string = "";
  MaximumRating: number = 0;
  is_recommended: boolean = false;

  constructor(private activatedRoute: ActivatedRoute, private service: ServicesService) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.id = params['id'];
    })
    this.service.getInterviewersDriveDetails(this.id).subscribe((data) => {
      this.drives = data.response;
      this.registrations = data.response.registrations;

    })

  }

  onSubmit() {
    this.feedback = new Feedback();
    this.feedback.IsRecommended = this.is_recommended;
    this.feedback.FeedBackComment = this.FeedBackComment;
    this.feedback.MaximumRating = this.MaximumRating;
    this.feedback.overallComments = this.overallComments;
    this.feedback.skillname = this.skillname;
    this.feedback.videourl = this.videourl;
    if (this.FeedBackComment.length <= 5) {
      this.FeedBackComment
      alert("FeedBackComment minimum 6 charaters ")
    }
    else if (this.overallComments.length <= 5) {
      alert("overallComments minimum 6 charaters ")
    }
    else if (this.skillname.length <= 1) {
      alert("skillname must not be empty")
    }
    else if (this.videourl.length <= 5) {
      alert("videour minimum 6 charaters ")
    }
    else if (isNaN(this.MaximumRating)) {
      alert("please enter MaximumRating is numbers only ")
    }
    else if (this.MaximumRating >= 69) {
      alert("please enter number range 0 to 69 only ")
    }
    else {
      console.log(this.feedback)
      this.clear();
    }
  }

  clear() {
    this.FeedBackComment = "";
    this.skillname = "";
    this.MaximumRating = 0;
    this.overallComments = "";
    this.videourl = "";
    this.is_recommended = false;
  }

}


