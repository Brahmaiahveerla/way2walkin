import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
details:any;
  constructor(private service: ServicesService) { }

  ngOnInit() {
    this.service.getprofile().subscribe((data) =>{
      this.details=data.response;
      
    })
  }

}
