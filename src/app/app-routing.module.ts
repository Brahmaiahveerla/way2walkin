import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginFormComponent } from './login-form/login-form.component';
import { EmployerDrivesComponent } from './Employer/employer-drives/employer-drives.component';
import { EmployerDriveDetailsComponent } from './Employer/employer-drive-details/employer-drive-details.component';
import { RegistrationComponent } from './registration/registration.component';
import { ProfileComponent } from './profile/profile.component';
import { InterviewDrivesComponent } from './interview/interview-drives/interview-drives.component';
import { InterviewerDriveDetailsComponent } from './interview/interviewer-drive-details/interviewer-drive-details.component';
import { InterviewgetfeedbackComponent } from './interview/interviewgetfeedback/interviewgetfeedback.component';
import {RecruiterDriveDetailsComponent} from'./Recruiter/recruiter-drive-details/recruiter-drive-details.component';
import {RecruiterCreateDriveComponent} from'./Recruiter/recruiter-create-drive/recruiter-create-drive.component';
import {RecruiterDriveCardsComponent} from './Recruiter/recruiter-drive-cards/recruiter-drive-cards.component'; 
const routes: Routes = [

  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginFormComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: 'Edrives', component: EmployerDrivesComponent },
  { path: 'DriveDetails', component: EmployerDriveDetailsComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'Idrives', component: InterviewDrivesComponent },
  { path: 'InterviewDrivegivefeedback', component: InterviewerDriveDetailsComponent },
  { path: 'InterviewDrivegetfeedback', component: InterviewgetfeedbackComponent },
  {path : 'Rdrivers',component : RecruiterDriveCardsComponent},
  {path : "article/:id", component :RecruiterDriveDetailsComponent},
 { path : 'createDrive',component : RecruiterCreateDriveComponent},

]; 

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
