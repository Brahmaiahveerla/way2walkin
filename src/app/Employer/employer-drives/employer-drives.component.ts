import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/services.service';

@Component({
  selector: 'app-employer-drives',
  templateUrl: './employer-drives.component.html',
  styleUrls: ['./employer-drives.component.css']
})
export class EmployerDrivesComponent implements OnInit {
 drives:any;
  constructor(private service: ServicesService) { }

  ngOnInit() {
    this.service.getEmployerDrives().subscribe((data) => {
      this.drives = data.response;
      console.log(this.drives) ;
    } 
    
    )
  }

}
