import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerDrivesComponent } from './employer-drives.component';

describe('EmployerDrivesComponent', () => {
  let component: EmployerDrivesComponent;
  let fixture: ComponentFixture<EmployerDrivesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerDrivesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerDrivesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
