import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ServicesService } from 'src/app/services.service';

@Component({
  selector: 'app-employer-drive-details',
  templateUrl: './employer-drive-details.component.html',
  styleUrls: ['./employer-drive-details.component.css']
})
export class EmployerDriveDetailsComponent implements OnInit {
  id: number;
  driveDetails: any;
  completedInterviews: any;

  scheduledInterviews: any;

  constructor(private activatedRoute: ActivatedRoute, private service: ServicesService) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.id = params['id'];
    })

    this.service.getDriveDetails(this.id).subscribe((data) => {
      this.driveDetails = data.response;
      console.log( this.driveDetails); 
    })


    this.service.getDriveCandidates(this.id).subscribe((data) => {
      this.completedInterviews = data.response.completedInterviews;
      this.scheduledInterviews = data.response.scheduledInterviews;
      console.log(data);
      console.log(this.completedInterviews);
      console.log(this.scheduledInterviews);
    })
  }
}