import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerDriveDetailsComponent } from './employer-drive-details.component';

describe('EmployerDriveDetailsComponent', () => {
  let component: EmployerDriveDetailsComponent;
  let fixture: ComponentFixture<EmployerDriveDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerDriveDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerDriveDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
