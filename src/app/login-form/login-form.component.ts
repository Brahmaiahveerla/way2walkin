import { Component, OnInit, ViewChild } from '@angular/core';
import { ServicesService } from '../services.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})

export class LoginFormComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  email_status: string;
  password_status: string;

  constructor(private formBuilder: FormBuilder, private service: ServicesService, private router: Router) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      emailId: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  valuechange(newValue) {
    this.password_status = " ";
    this.email_status = " ";
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    this.service.doLogin(this.registerForm.value).subscribe((data) => {

      if (data.statusCode == 1) {
        localStorage.setItem('jwt-token', data.token);
        localStorage.setItem('Role', data.response.role);
        localStorage.setItem('userId', data.response.userId);
        window.location.reload();

        if (localStorage.getItem('Role') == '2') {
          this.router.navigate(['/Edrives'])

        }
        else if (localStorage.getItem('Role') == '3') {
          this.router.navigate(['/Idrives'])
        }
        else if (localStorage.getItem('Role') == '4') {
          this.router.navigate(['/Rdrives'])
        }
      }
      else if (data.statusCode == 6) {
        this.email_status = "wrong Email";
      }
      else if (data.statusCode == 3) {
        this.password_status = "wrong password";
      }
    })


  }
}

