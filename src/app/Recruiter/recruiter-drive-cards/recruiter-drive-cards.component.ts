import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/services.service';
@Component({
  selector: 'app-recruiter-drive-cards',
  templateUrl: './recruiter-drive-cards.component.html',
  styleUrls: ['./recruiter-drive-cards.component.css']
})
export class RecruiterDriveCardsComponent implements OnInit {
  data1 : any;
  constructor(private service: ServicesService) { }

  fetchRecuriterDrives(){
    this.service.getRecuriterDrives().subscribe(data => {
      this.data1 = data.response; 
    })
 }
 ngOnInit() {
   this.fetchRecuriterDrives();
 }   

}

  

  
