import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterDriveCardsComponent } from './recruiter-drive-cards.component';

describe('RecruiterDriveCardsComponent', () => {
  let component: RecruiterDriveCardsComponent;
  let fixture: ComponentFixture<RecruiterDriveCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecruiterDriveCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterDriveCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
