import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterDriveDetailsComponent } from './recruiter-drive-details.component';

describe('RecruiterDriveDetailsComponent', () => {
  let component: RecruiterDriveDetailsComponent;
  let fixture: ComponentFixture<RecruiterDriveDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecruiterDriveDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterDriveDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
