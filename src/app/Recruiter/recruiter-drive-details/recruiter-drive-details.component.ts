import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/services.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-recruiter-drive-details',
  templateUrl: './recruiter-drive-details.component.html',
  styleUrls: ['./recruiter-drive-details.component.css']
})
export class RecruiterDriveDetailsComponent implements OnInit {
  singleDrive: any;
  singleDriveDetails: boolean = false;
  registrations : any;
  registeredCandidates : any;
  usetrRole: any = localStorage.getItem('userId');
  completedInterviews: any;
  interviews: any;
  sheduledInterviews: any;
  drill_down_feedback: any;
  candidate_feedback: any;
    

  constructor(private service: ServicesService,private route: ActivatedRoute) { }
 
  fetchdriveById(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.service.getDrivesById(id).subscribe(singleDrive => {
      this.singleDrive = singleDrive;
      console.log(this.singleDrive);
      this.registrations = singleDrive.response.registrations;
      console.log( this.registrations);
      
  })
    this.singleDriveDetails = true;
    // this.registeredCandidates = this.singleDrive.response.registrations.filter((candidate) => candidate.is_scheduled == false && candidate.is_accepted == false);
  }
  

  receiveInterViewsById(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.service.getCompletedInerviews(id).subscribe(completedInterviews => {
      this.completedInterviews = completedInterviews;
      this.interviews = completedInterviews.response.completedInterviews;
      this.sheduledInterviews = completedInterviews.response.scheduledInterviews;
      console.log(this.sheduledInterviews.length);
      if (this.interviews) {
        console.log(this.interviews);
        this.interviews.map(candidatelist => {
          this.candidate_feedback = candidatelist.feedBack;
          console.log(this.candidate_feedback);
        })
      }
      if (this.candidate_feedback) {
        this.drill_down_feedback = this.candidate_feedback.drill_down_feedback;
        console.log(this.drill_down_feedback);
      }
    })
   
  }
  ngOnInit() {
    this.fetchdriveById();
    this.receiveInterViewsById();
  }


}







  
  

 


