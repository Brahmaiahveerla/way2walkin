import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterCreateDriveComponent } from './recruiter-create-drive.component';

describe('RecruiterCreateDriveComponent', () => {
  let component: RecruiterCreateDriveComponent;
  let fixture: ComponentFixture<RecruiterCreateDriveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecruiterCreateDriveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterCreateDriveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
