
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ServicesService } from 'src/app/services.service';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-recruiter-create-drive',
  templateUrl: './recruiter-create-drive.component.html',
  styleUrls: ['./recruiter-create-drive.component.css']
})
export class RecruiterCreateDriveComponent implements OnInit {

  @ViewChild('f') createDriveForm : NgForm;

  allEmployeesList : any;
  newDriveData = {
  "heading":"",
  "sub_heading": "",
  "description":" ",
  "city":"",
  "exactly":"",
  "when":"",
  "url":"",
  "eligibility":" ",
  "more_information":" ",
  "phone":"",
  "where":"",
  "tags":"",
  "employers":[{"userId":""}],
  "field":
   { 
  "heading":"",
  "sub_heading":"",
  "description":"",
  "city":"",
  "exactly":"",
  "when":"",
  "url":" ",
  "eligibility":" ",
  "more_information":" ",
  "phone":" ",
  "where":" ",
  "tags":"",
  "employers":[{"userId":""}]
  },
  "id":""
    
  }

  

  constructor(private service: ServicesService) { }

  //method for creating new drive 
   newDrive() :void{
     debugger;
    this.service.createnewDrive(this.newDriveData).subscribe(resultofNewDrive => {
      console.log(resultofNewDrive);
    })
  }

 
   
  //method for fetch all employees
  fetchAllEmployees(){
        this.service.getAllEmployeers().subscribe(data => {
        this.allEmployeesList = data.response; 
        console.log(this.allEmployeesList);
      })
   }

   onSubmit(){
     console.log(this.createDriveForm.value);
     this.newDriveData.heading = this.createDriveForm.value.heading;
     this.newDriveData.sub_heading = this.createDriveForm.value.subHeading;
     this.newDriveData.description = this.createDriveForm.value.description;
     this.newDriveData.city = this.createDriveForm.value.city;
     this.newDriveData.exactly = this.createDriveForm.value.exactly;
     this.newDriveData.when = this.createDriveForm.value.when;
     this.newDriveData.url = this.createDriveForm.value.url;
     this.newDriveData.eligibility = this.createDriveForm.value.eligibility;
     this.newDriveData.more_information = this.createDriveForm.value.more;
     this.newDriveData.phone = this.createDriveForm.value.phone;
     this.newDriveData.where = this.createDriveForm.value.where;
     this.newDriveData.tags = this.createDriveForm.value.tags;
     this.newDriveData.employers[0].userId = this.createDriveForm.value.selectedValue;

     this.newDriveData.field.heading = this.createDriveForm.value.heading;
     this.newDriveData.field.sub_heading = this.createDriveForm.value.subHeading;
     this.newDriveData.field.description = this.createDriveForm.value.description;
     this.newDriveData.field.city = this.createDriveForm.value.city;
     this.newDriveData.exactly = this.createDriveForm.value.exactly;
     this.newDriveData.field.when = this.createDriveForm.value.when;
     this.newDriveData.field.url = this.createDriveForm.value.url;
     this.newDriveData.field.eligibility = this.createDriveForm.value.eligibility;
     this.newDriveData.field.more_information = this.createDriveForm.value.more;
     this.newDriveData.field.phone = this.createDriveForm.value.phone;
     this.newDriveData.field.where = this.createDriveForm.value.where;
     this.newDriveData.field.tags = this.createDriveForm.value.tags;
     this.newDriveData.field.employers[0].userId = this.createDriveForm.value.selectedValue;
     this.newDriveData.id = localStorage.getItem("userId");

     this.newDrive();
    // console.log(this.newDriveData);
   }

  ngOnInit() {
  this.fetchAllEmployees();
  }

}
